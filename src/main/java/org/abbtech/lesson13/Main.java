package org.abbtech.lesson13;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> userRepository = new ArrayList<>();
        userRepository.add(new User(11562323, "Elvin", true));
        userRepository.add(new User(41216133, "Aydan", false));

        UserService userService = new UserServiceImpl(userRepository);

        try {
            System.out.println("Is Elvin active? " + userService.isUserActive("Elvin"));
            System.out.println("Is Aydan active? " + userService.isUserActive("Aydan"));
            System.out.println("Is Nigar active? " + userService.isUserActive("Nigar"));
            userService.deleteUser(41216133);
            System.out.println("Deleted Elvin");
            System.out.println("Is Elvin active? " + userService.isUserActive("Elvin"));
            userService.getUser(11562323);
            System.out.println("Retrieved Aydan");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
