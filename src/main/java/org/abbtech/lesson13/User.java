package org.abbtech.lesson13;

public class User {
    private int userId;
    private String username;
    private boolean active;

    public User(int userId, String username, boolean active) {
        this.userId = userId;
        this.username = username;
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public int getUserId() {
        return userId;
    }

    public boolean isActive() {
        return active;
    }

}
