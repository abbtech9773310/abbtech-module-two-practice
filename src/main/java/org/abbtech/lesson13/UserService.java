package org.abbtech.lesson13;

public interface UserService {
    boolean isUserActive(String username);

    void deleteUser(int userId) throws Exception;

    User getUser(int userId) throws Exception;
}
