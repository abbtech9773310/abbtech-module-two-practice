package org.abbtech.lesson13;

import java.util.List;

public class UserServiceImpl implements UserService {
    private List<User> userRepository;

    public UserServiceImpl(List<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean isUserActive(String username) {
        User user = userRepository.stream()
                .filter(u -> u.getUsername().equals(username))
                .findFirst()
                .orElse(null);
        return user != null && user.isActive();
    }

    @Override
    public void deleteUser(int userId) throws Exception {
        User user = userRepository.stream()
                .filter(u -> u.getUserId() == userId)
                .findFirst()
                .orElse(null);
        if (user == null) {
            throw new Exception("User not found");
        }
        userRepository.remove(user);
    }

    @Override
    public User getUser(int userId) throws Exception {
        User user = userRepository.stream()
                .filter(u -> u.getUserId() == userId)
                .findFirst()
                .orElse(null);

        if (user == null) {
            throw new Exception("User not found");
        }
        return user;
    }
}
