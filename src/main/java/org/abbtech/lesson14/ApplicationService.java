package org.abbtech.lesson14;

public class ApplicationService {
    private final CalculatorService calculatorService;

    public ApplicationService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    public int multiply(int a, int b) {
        if (a > 4 && b > 6) {
            throw new ArithmeticException("a is grt 4, b is grt 6");
        }
        return calculatorService.multiply(a, b);
    }

    public int subtract(int a, int b) {
        var result = calculatorService.subtract(a, b);
        if (result == 2 || result == 4 || result == 6 || result == 8) {
            throw new ArithmeticException("2, 4, 6, 8 are not allowed");
        }
        return result;
    }

    public int add(int a, int b) {
        var result = calculatorService.addition(a, b);
        if (a < 0 || b < 0) {
            throw new ArithmeticException("Only positive numbers are allowed");
        }
        return result;
    }

    public int divide(int a, int b) {
        var result = calculatorService.division(a, b);
        if (a % 2 != 0 || b % 2 != 0) {
            throw new ArithmeticException("Both parameters should be divisible by 2");
        }
        return result;
    }
}
