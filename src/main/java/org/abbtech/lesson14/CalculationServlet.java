package org.abbtech.lesson14;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/add", "/calculator/subtract", "/calculator/multiply", "/calculator/divide"})
public class CalculationServlet extends HttpServlet {

    private ApplicationService applicationService;

    @Override
    public void init() throws ServletException {
        super.init();
        applicationService = new ApplicationService(new CalculatorServiceImpl());
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String calculationMethod = req.getHeader("x-calculation-method");
        int a = Integer.parseInt(req.getParameter("a"));
        int b = Integer.parseInt(req.getParameter("b"));
        int result = 0;
        PrintWriter writer = resp.getWriter();

        try {

            switch (calculationMethod) {
                case "multiply":
                    result = applicationService.multiply(a, b);
                    break;
                case "subtract":
                    result = applicationService.subtract(a, b);
                    break;
                case "add":
                    result = applicationService.add(a, b);
                    break;
                case "divide":
                    result = applicationService.divide(a, b);
                    break;
            }


            writer.write("""
                    {
                            "result":""" + result + """
                                
                          }
                    """);
        } catch (ArithmeticException arithmeticException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                    {
                        "exception": "INVALID ARGUMENTS"
                     }                    
                     """);
        }
        resp.setContentType("application/json");
    }
}
