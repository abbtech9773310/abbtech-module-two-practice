package org.abbtech.lesson14;

public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public int multiply(int a, int b) {
        return a * b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }

    @Override
    public int addition(int a, int b) {
        return a + b;
    }

    @Override
    public int division(int a, int b) {
        return a / b;
    }
}
