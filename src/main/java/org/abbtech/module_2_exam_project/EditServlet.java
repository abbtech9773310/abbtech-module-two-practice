package org.abbtech.module_2_exam_project;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h1>Update Task</h1>");
        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);

        Task t = TaskDAO.getTaskById(id);

        out.print("<form action='EditServlet2' method='post'>");
        out.print("<table>");
        out.print("<tr><td></td><td><input type='hidden' name='id' value='" + t.getId() + "'/></td></tr>");
        out.print("<tr><td>Username:</td><td><input type='text' name='username' value='" + t.getUsername() + "'/></td></tr>");
        out.print("<tr><td>TaskTitle:</td><td><input type='taskTitle' name='taskTitle' value='" + t.getTaskTitle() + "'/> </td></tr>");
        out.print("<tr><td>Task:</td><td><input type='task' name='task' value='" + t.getTask() + "'/></td></tr>");
        out.print("<tr><td colspan='2'><input type='submit' value='Edit & Save '/></td></tr>");
        out.print("</table>");
        out.print("</form>");
        out.close();
    }
}
