package org.abbtech.module_2_exam_project;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/EditServlet2")
public class EditServlet2 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);
        String username = request.getParameter("username");
        String taskTitle = request.getParameter("taskTitle");
        String task = request.getParameter("task");

        Task t = new Task();
        t.setId(id);
        t.setUsername(username);
        t.setTaskTitle(taskTitle);
        t.setTask(task);

        int status = TaskDAO.update(t);
        if (status > 0) {
            response.sendRedirect("ViewServlet");
        } else {
            out.println("Sorry! unable to update task");
        }

        out.close();
    }

}