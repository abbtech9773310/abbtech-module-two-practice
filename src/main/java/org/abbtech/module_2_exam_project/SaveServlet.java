package org.abbtech.module_2_exam_project;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String username = request.getParameter("username");
        String taskTitle = request.getParameter("taskTitle");
        String task = request.getParameter("task");

        Task t = new Task();
        t.setUsername(username);
        t.setTaskTitle(taskTitle);
        t.setTask(task);


        int status = TaskDAO.save(t);
        if (status > 0) {
            out.print("<p>Task saved successfully!</p>");
            request.getRequestDispatcher("index.jsp").include(request, response);
        } else {
            out.println("Sorry! unable to save the task");
        }

        out.close();
    }

}