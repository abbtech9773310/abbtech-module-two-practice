package org.abbtech.module_2_exam_project;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TaskDAO {


    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres", "psg_user", "pass");
        } catch (Exception e) {
            System.out.println(e);
        }
        return con;
    }

    public static int save(Task t) {
        int status = 0;
        try {
            Connection con = TaskDAO.getConnection();
            PreparedStatement ps = con.prepareStatement(
                    "insert into tasks(username,taskTitle,task) values (?,?,?)");
            ps.setString(1, t.getUsername());
            ps.setString(2, t.getTaskTitle());
            ps.setString(3, t.getTask());

            status = ps.executeUpdate();

            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return status;
    }

    public static int update(Task t) {
        int status = 0;
        try {
            Connection con = TaskDAO.getConnection();
            PreparedStatement ps = con.prepareStatement(
                    "update tasks set username=?,taskTitle=?,task=? where id=?");
            ps.setString(1, t.getUsername());
            ps.setString(2, t.getTaskTitle());
            ps.setString(3, t.getTask());
            ps.setInt(4, t.getId());

            status = ps.executeUpdate();

            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return status;
    }

    public static int delete(int id) {
        int status = 0;
        try {
            Connection con = TaskDAO.getConnection();
            PreparedStatement ps = con.prepareStatement("delete from tasks where id=?");
            ps.setInt(1, id);
            status = ps.executeUpdate();

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public static Task getTaskById(int id) {
        Task t = new Task();

        try {
            Connection con = TaskDAO.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from tasks where id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                t.setId(rs.getInt(1));
                t.setUsername(rs.getString(2));
                t.setTaskTitle(rs.getString(3));
                t.setTask(rs.getString(4));
            }
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return t;
    }

    public static List<Task> getAllTasks() {
        List<Task> list = new ArrayList<Task>();

        try {
            Connection con = TaskDAO.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from tasks");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Task t = new Task();
                t.setId(rs.getInt(1));
                t.setUsername(rs.getString(2));
                t.setTaskTitle(rs.getString(3));
                t.setTask(rs.getString(4));
                list.add(t);
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
}
