package org.abbtech.module_2_exam_project;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<a href='index.jsp'>Add New Task</a>");
        out.println("<h1>Task List</h1>");

        List<Task> list = TaskDAO.getAllTasks();

        out.print("<table border='1' width='100%'");
        out.print("<tr><th>Id</th><th>Username</th><th>TaskTitle</th><th>Task</th><th>Edit</th><th>Delete</th></tr>");
        for (Task t : list) {
            out.print("<tr><td>" + t.getId() + "</td><td>" + t.getUsername() + "</td><td>" + t.getTaskTitle() + "</td> <td>" + t.getTask() + "</td><td><a href='EditServlet?id=" + t.getId() + "'>edit</a></td> <td><a href='DeleteServlet?id=" + t.getId() + "'>delete</a></td></tr>");
        }
        out.print("</table>");

        out.close();
    }
}
