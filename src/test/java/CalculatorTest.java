import org.abbtech.lesson1.Calculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }


    @Test
    public void testAddition() {
        Calculator calculator = new Calculator();
        assertEquals(5, calculator.addition(2, 3));
        assertEquals(-1, calculator.addition(-2, 1));
        assertEquals(0, calculator.addition(0, 0));
        assertEquals(1000000000, calculator.addition(500000000, 500000000));
    }

    @Test
    public void testSubtraction() {
        Calculator calculator = new Calculator();
        assertEquals(1, calculator.subtraction(3, 2));
        assertSame(-3, calculator.subtraction(-1, 2));
        assertEquals(0, calculator.subtraction(0, 0));
        assertEquals(0, calculator.subtraction(500000000, 500000000));
    }

    @Test
    public void testMultiplication() {
        Calculator calculator = new Calculator();
        assertEquals(6, calculator.multiplication(2, 3));
        assertSame(-2, calculator.multiplication(-1, 2));
        assertEquals(0, calculator.multiplication(0, 0));
        assertNotEquals(250000000000000000L, calculator.multiplication(500000000, 500000000));
    }

    @Test
    public void testDivision() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.division(6, 3));
        assertEquals(-2, calculator.division(-4, 2));
        assertThrows(ArithmeticException.class, () -> calculator.division(5, 0));
    }


}
