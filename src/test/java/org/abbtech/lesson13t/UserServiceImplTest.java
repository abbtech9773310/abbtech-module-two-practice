package org.abbtech.lesson13t;

import org.abbtech.lesson13.User;
import org.abbtech.lesson13.UserService;
import org.abbtech.lesson13.UserServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceImplTest {

    @Test
    void testIsUserActive() {
        List<User> userRepository = new ArrayList<>();
        userRepository.add(new User(22, "Samir", true));

        UserService userService = new UserServiceImpl(userRepository);
        assertTrue(userService.isUserActive("Samir"));
        assertFalse(userService.isUserActive("Orxan"));
    }

    @Test
    void testDeleteUser() throws Exception {
        List<User> userRepository = new ArrayList<>();
        userRepository.add(new User(33, "Orxan", true));

        UserService userService = new UserServiceImpl(userRepository);
        userService.deleteUser(33);
        assertTrue(userRepository.isEmpty());
    }

    @Test
    void testGetUser() throws Exception {
        List<User> userRepository = new ArrayList<>();
        User user = new User(44, "Elvin", true);
        userRepository.add(user);

        UserService userService = new UserServiceImpl(userRepository);
        assertEquals(user, userService.getUser(44));
    }
}
